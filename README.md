# ssm-eclipse-chapter11
![输入图片说明](https://images.gitee.com/uploads/images/2021/0414/095906_8d6aa78c_382074.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0414/102536_dd86310d_382074.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0414/102621_26a8d810_382074.png "屏幕截图.png")
#### 介绍
SpringMVC入门 P173

#### 数据库版本为8.0所以驱动名称和url都有所改动
#### 导入了fastjson jar包用于将数据格式化为JSON字符串

#### 软件架构
B/S模式，缺省使用的 SpringMVC 4.3 + Tomcat 8，注意根据具体环境，修改tomcat配置

Bootstrap 学习：
 https://www.bilibili.com/video/BV1R7411s72K?p=2
 https://www.bilibili.com/video/BV1TU4y1p7zU?from=search&seid=8215767203503326072

https://www.runoob.com/bootstrap/bootstrap-tutorial.html


LayUI 学习： https://www.bilibili.com/video/BV1fy4y1S7P8?p=17&spm_id_from=pageDriver

Spring 学习：https://www.bilibili.com/video/BV1Q54y1z7mY

ElementUI : https://www.bilibili.com/video/BV1y5411p7DX?p=7

#### 安装教程

1.  查找 tomcat 配置

2.  修改 ProjectFacet 和 Project properties

![输入图片说明](https://images.gitee.com/uploads/images/2021/0407/172144_de391e93_382074.png "屏幕截图.png")

Project properties - build path 

![输入图片说明](https://images.gitee.com/uploads/images/2021/0407/172349_8cf4eb8a_382074.png "屏幕截图.png")


#### 使用说明

1.  使用J2EE视图，Run on Server 


2.  浏览器访问  http://localhost:8080/chapter11/firstController
![输入图片说明](https://images.gitee.com/uploads/images/2020/1101/174753_1528f93d_382074.png "屏幕截图.png")

