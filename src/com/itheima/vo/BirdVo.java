package com.itheima.vo;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itheima.po.Bird;

public class BirdVo {

	private Bird bird;
	private String url;
	
	public Bird getBird() {
		return bird;
	}
	public void setBird(Bird bird) {
		this.bird = bird;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "BirdVo [bird=" + bird + ", url=" + url + "]";
	}

	public List<BirdVo> transfer(List<Bird> birdList){
		return null;
	}
	
	/**
	 * 获取 对象的Json格式
	 * @param birdVo
	 * @return String
	 */
	public String getJson(BirdVo birdVo){
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = null;
		try {
			jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(birdVo);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return jsonString;		
	}

}
