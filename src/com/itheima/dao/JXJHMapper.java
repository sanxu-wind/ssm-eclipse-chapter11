package com.itheima.dao;

import java.util.List;

import com.itheima.po.Bird;
import com.itheima.po.JXJH;

public interface JXJHMapper {
	public List<JXJH> findAllJXJH();
	public List<JXJH> findJxjhByJSXM(String jsxm);
	public List<JXJH> findJxjhByKKBJ(String kkbj);
	public List<JXJH> findJxjhByKCMC(String kcmc);
}
