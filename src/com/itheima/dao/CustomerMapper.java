package com.itheima.dao;
import java.util.List;

import com.itheima.po.Customer;
public interface CustomerMapper {
	// 通过id查询客户
	public Customer findCustomerById(Integer id);
	
	// 查询所有客户
	public List<Customer> findCustomerAll();

}
