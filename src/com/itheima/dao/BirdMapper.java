package com.itheima.dao;

import java.util.List;

import com.itheima.po.Bird;

public interface BirdMapper {
	public List<Bird> findBirdAll();
}
