package com.itheima.test;
import java.util.List;

import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.itheima.dao.CustomerMapper;
import com.itheima.dao.JXJHMapper;
import com.itheima.po.Customer;
import com.itheima.po.JXJH;
/**
 * 入门程序测试类
 */
public class JXJHTest {


	@Test
	public void findAllJXJHTest() {
	    ApplicationContext act = 
	            new ClassPathXmlApplicationContext("applicationContext.xml");
	    JXJHMapper jxjhMapper = act.getBean(JXJHMapper.class); 
	    
		// SqlSession执行映射文件中定义的SQL，并返回映射结果
		List<JXJH> list = jxjhMapper.findAllJXJH();
		for (JXJH jxjh : list) {
			System.out.println(jxjh);
		}

	}
	
	//按教师姓名查询
	@Test
	public void findJXJHByJSXMTest() {
	    ApplicationContext act = 
	            new ClassPathXmlApplicationContext("applicationContext.xml");
	    JXJHMapper jxjhMapper = act.getBean(JXJHMapper.class); 
		List<JXJH> list = jxjhMapper.findJxjhByJSXM("魏星");
		for (JXJH jxjh : list) {
			System.out.println(jxjh);
		}
	}
	
	//按开课班级查询
	@Test
	public void findJXJHByKKBJTest() {
	    ApplicationContext act = 
	            new ClassPathXmlApplicationContext("applicationContext.xml");
	    JXJHMapper jxjhMapper = act.getBean(JXJHMapper.class); 		
		List<JXJH> list = jxjhMapper.findJxjhByKKBJ("18级软件");
		for (JXJH jxjh : list) {
			System.out.println(jxjh);
		}
	}
	
	@Test
	public void findJXJHByKCMCTest() {
	    ApplicationContext act = 
	            new ClassPathXmlApplicationContext("applicationContext.xml");
	    JXJHMapper jxjhMapper = act.getBean(JXJHMapper.class); 	
		List<JXJH> list = jxjhMapper.findJxjhByKCMC("Java");
		for (JXJH jxjh : list) {
			System.out.println(jxjh);
		}

	}
	
}