package com.itheima.test;

import java.util.List;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.itheima.dao.BirdMapper;
import com.itheima.dao.CustomerMapper;
import com.itheima.po.Bird;
import com.itheima.po.Customer;
import com.itheima.service.BirdService;
import com.itheima.service.BirdServiceImpl;
import com.itheima.vo.BirdVo;


public class BirdTest {

	@Test
	public void findCustomerByIdDaoTest(){	
	    ApplicationContext act = 
	            new ClassPathXmlApplicationContext("applicationContext.xml");
	    CustomerMapper customerMapper = act.getBean(CustomerMapper.class);   
	    Customer customer = customerMapper.findCustomerById(1);
	    System.out.println(customer);
	}
	
	@Test
	public void findBirdAllDaoTest() throws Exception {
	    ApplicationContext act = 
	            new ClassPathXmlApplicationContext("applicationContext.xml");
	    BirdMapper birdMapper = act.getBean(BirdMapper.class);   
	    List<Bird> birdList = birdMapper.findBirdAll();
		

		for (Bird bird : birdList) {
			System.out.println(bird);
		}
	}
	
	@Test
	public void findBirdAllServiceTest() throws Exception {
		ApplicationContext act = 
				new ClassPathXmlApplicationContext("applicationContext.xml");
		BirdService birdService = act.getBean(BirdServiceImpl.class);   
		List<Bird> birdList = birdService.findBirdAll();

		for (Bird bird : birdList) {
			System.out.println(bird);
		}
	}

	@Test
	public void testBirdVoJson(){
		Bird bird = new Bird();
		bird.setId(1);
		bird.setType("乌鸡");
		BirdVo birdVo = new BirdVo();
		birdVo.setBird(bird);
		birdVo.setUrl("http://www.baidu.com");
		
		System.out.println(birdVo.getJson(birdVo));
	}
}
