package com.itheima.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.itheima.po.Bird;
import com.itheima.service.BirdService;
import com.mysql.cj.xdevapi.JsonArray;

@Controller
public class BirdPageController {

	@RequestMapping("/birdpage")
	@ResponseBody
	public JSONArray getList() {
		
		ApplicationContext act = 
	            new ClassPathXmlApplicationContext("applicationContext.xml");
	    BirdService birdService = act.getBean(BirdService.class); 
	    
	    List<Bird> birdList = birdService.findBirdAll();
	    JSONArray array = JSONArray.parseArray(JSON.toJSONString(birdList));
		
		return array;
	}
	
}
