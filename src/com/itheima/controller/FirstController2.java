package com.itheima.controller;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
/**
 * 控制器类
 */
@Controller
public class FirstController2{
	@RequestMapping(value="/firstController2")
	public ModelAndView handleRequest(HttpServletRequest request,
			               HttpServletResponse response)  {
         // 创建ModelAndView对象
		ModelAndView mav = new ModelAndView();
         // 向模型对象中添加数据
		mav.addObject("msg", "这是我的第二个Spring MVC程序");
         // 设置逻辑视图名
		mav.setViewName("/WEB-INF/jsp/first.jsp");
         // 返回ModelAndView对象
		return mav;
	}
	@RequestMapping(value="/first2")
	public String handleRequest2(HttpServletRequest request,
			HttpServletResponse response,Model model)  {
		model.addAttribute("msg", "这是我的第二个Spring MVC程序");
		return "/WEB-INF/jsp/first.jsp";
	}
}
