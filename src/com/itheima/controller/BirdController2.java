package com.itheima.controller;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.itheima.po.Bird;
import com.itheima.service.BirdService;
/**
 * 控制器类
 */
@Controller
public class BirdController2 {
	@Autowired
	private BirdService birdService;
	
	@RequestMapping("/birdController2")
	public ModelAndView handleRequest(HttpServletRequest request,
			               HttpServletResponse response)  {
		
//	    ApplicationContext act = 
//	            new ClassPathXmlApplicationContext("applicationContext.xml");
//	    BirdService birdService = act.getBean(BirdService.class); 
	    
	    List<Bird> birdList = this.birdService.findBirdAll();
	    
         // 创建ModelAndView对象
		ModelAndView mav = new ModelAndView();
		
         // 向模型对象中添加数据
		mav.addObject("birdList", birdList);
         // 设置逻辑视图名
		mav.setViewName("/WEB-INF/jsp/bird.jsp");
         // 返回ModelAndView对象
		return mav;
	}
}
