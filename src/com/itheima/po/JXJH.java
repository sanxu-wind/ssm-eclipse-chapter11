package com.itheima.po;

public class JXJH {

	//序号
	private Integer id;
	
	//课程名称
	private String kcmc;
	//学分	
	//周学时
	//周数
	//总课时
	//课程分类
	//考核方式
	//开课学院
	
	//开课班级
	private String kkbj;
	
	//合班情况
	//上课人数
	
	public String getKkbj() {
		return kkbj;
	}

	public void setKkbj(String kkbj) {
		this.kkbj = kkbj;
	}

	//上课教师
	private String skjs;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getKcmc() {
		return kcmc;
	}

	public void setKcmc(String kcmc) {
		this.kcmc = kcmc;
	}

	public String getSkjs() {
		return skjs;
	}

	public void setSkjs(String skjs) {
		this.skjs = skjs;
	}

	@Override
	public String toString() {
		return "JXJH [id=" + id + ", kcmc=" + kcmc + ", kkbj=" + kkbj + ", skjs=" + skjs + "]";
	}


	
	//课程负责人
	//private String kcfzr;
	
	//原教师
	//是否外聘
	//教室类型
	//备注
	//学期
	
	//校区
	//private String xq;


 
}
