package com.itheima.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.itheima.dao.BirdMapper;
import com.itheima.po.Bird;
import com.itheima.vo.BirdVo;

@Service
public class BirdServiceImpl implements BirdService{
	@Autowired
	private BirdMapper birdMapper;
	
	public List<Bird> findBirdAll(){   
		List<Bird> birdList = this.birdMapper.findBirdAll();
		return birdList;
	}
	
	public List<BirdVo> findBirdVoAll(){
		List<Bird> birdList = this.birdMapper.findBirdAll();
		List<BirdVo> birdVoList = new ArrayList<BirdVo>();
		//添加转换逻辑
		
		return birdVoList;
	}
}


