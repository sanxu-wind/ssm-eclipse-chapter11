package com.itheima.service;

import java.util.List;

import com.itheima.po.Bird;
import com.itheima.vo.BirdVo;

public interface BirdService {
	public List<Bird> findBirdAll();
	
	public List<BirdVo> findBirdVoAll();
}
