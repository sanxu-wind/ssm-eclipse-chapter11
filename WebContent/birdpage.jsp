<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh">
<head>
<meta charset="UTF-8">
<title>主页-bootstrap(临时的)</title>
<!-- 新 Bootstrap 核心 CSS 文件 -->
<link
	href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet">

<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>

<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
<script
	src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="//unpkg.com/layui@2.6.5/dist/css/layui.css">
</head>

<body>
	<fieldset class="layui-elem-field" style="margin: 5%">
		<legend>乌鸦凤凰分页表</legend>
		<div class="layui-field-box" style="margin: 1%">
			<table class="layui-table" id="demo" lay-filter="test"></table>
		</div>
	</fieldset>
</body>
<script src="//unpkg.com/layui@2.6.5/dist/layui.js"></script>
<script type="text/javascript">
	
	layui.define(function(){
		
		var table = layui.table;
		var $ = layui.jquery;
		let tableData = [];
		
		//	表格渲染模板，开启分页
		table.render({
            elem: '#demo',
            cols: [[ //表头
                {field: 'id', title: 'ID', sort: true, fixed: 'left'},
                {field: 'type', title: '鸟类'}
            ]],
            page: true,
            limit: 10,
            limits: [5,10,15,20,50]
        });
		
		$.ajax({
			url: "${pageContext.request.contextPath}/birdpage",
			type: "post",
			data: {},
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			success: function(data){
				tableData = data;
				//	请求获得的数据作为表格静态数据，进行表格重载
				layui.table.reload('demo',{
                    data: tableData
                });
				
			}
		})
	})
	
</script>
</html>
