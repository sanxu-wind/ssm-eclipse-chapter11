<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh">
<head>
<meta charset="UTF-8">
<title>主页-bootstrap(临时的)</title>
<!-- 新 Bootstrap 核心 CSS 文件 -->
<link
	href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet">

<!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
<script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>

<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
<script
	src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
	<div class="h1">第11章<span class="small">SpringMVC入门</span></div>
	<br>
	<div class="row clearfix">
			<div class="row">
				<div class="col-md-4">
					<div class="thumbnail">
						<div class="caption">
							<h3>控制器 firstController</h3>
							<p>演示 firstController -> first.jsp</p>
							<p>
								<a class="btn btn-primary" href="firstController">点我</a>
							</p>
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="thumbnail">
						<div class="caption">
							<h3>控制器 birdController</h3>
							<p>演示 birdController -> bird.jsp</p>
							<p>
								<a class="btn btn-primary" href="birdController">点我</a>
							</p>
						</div>
					</div>
				</div>
			</div>
	</div>
	</div>
</body>
</html>
