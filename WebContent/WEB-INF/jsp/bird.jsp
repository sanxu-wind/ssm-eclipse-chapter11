<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>鸟信息</title>
<link rel="stylesheet" type="text/css"
	href="https://www.layuicdn.com/layui-v2.5.6/css/layui.css" />

</head>
<body>
	<table class="layui-table">
		<colgroup>
			<col width="50">
			<col width="100">
		</colgroup>
		<thead>
			<tr>
				<th>编号</th>
				<th>名称</th>
			</tr>
		</thead>

		<tbody>
			<c:forEach items="${birdList}" var="bird" varStatus="status">
				<tr>
					<td>${bird.id}</td>
					<td>${bird.type}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<script src="https://www.layuicdn.com/layui-v2.5.6/layui.js"></script>

</body>
<script type="text/javascript">
	console.log(${bird.list});
</script>
</html>